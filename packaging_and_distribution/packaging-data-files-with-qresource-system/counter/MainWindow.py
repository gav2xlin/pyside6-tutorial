# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.4.3
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QLabel, QMainWindow, QMenuBar,
    QPushButton, QSizePolicy, QStatusBar, QVBoxLayout,
    QWidget)
import resources_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(398, 413)
        MainWindow.setAcceptDrops(False)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setPointSize(28)
        font.setBold(True)
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.btn_inc = QPushButton(self.centralwidget)
        self.btn_inc.setObjectName(u"btn_inc")
        icon = QIcon()
        icon.addFile(u":/icons/plus.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_inc.setIcon(icon)

        self.verticalLayout.addWidget(self.btn_inc)

        self.btn_dec = QPushButton(self.centralwidget)
        self.btn_dec.setObjectName(u"btn_dec")
        icon1 = QIcon()
        icon1.addFile(u":/icons/minus.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_dec.setIcon(icon1)

        self.verticalLayout.addWidget(self.btn_dec)

        self.btn_reset = QPushButton(self.centralwidget)
        self.btn_reset.setObjectName(u"btn_reset")
        icon2 = QIcon()
        icon2.addFile(u":/icons/arrow-return-180-left", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_reset.setIcon(icon2)

        self.verticalLayout.addWidget(self.btn_reset)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 398, 26))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Counter", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.btn_inc.setText(QCoreApplication.translate("MainWindow", u"Increment", None))
        self.btn_dec.setText(QCoreApplication.translate("MainWindow", u"Decrement", None))
        self.btn_reset.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
    # retranslateUi

