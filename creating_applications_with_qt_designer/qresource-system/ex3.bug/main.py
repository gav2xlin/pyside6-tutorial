import sys

from PySide6.QtWidgets import QApplication, QMainWindow

# pyside6-uic form.ui -o ui_form.py
# pyside2-uic form.ui -o ui_form.py

# pyside6-uic mainwindow.ui -o MainWindow.py
# pyside6-rcc resources.qrc -o resources_rc.py

from MainWindow import Ui_MainWindow


class AppWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = AppWindow()
    widget.show()
    sys.exit(app.exec())
