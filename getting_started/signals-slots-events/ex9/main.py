import sys

from PySide6.QtWidgets import QApplication, QLabel, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.label = QLabel("Click in this window")
        self.setCentralWidget(self.label)

    def mouseMoveEvent(self, e):
        self.label.setText("mouseMoveEvent")
        # super().mouseMoveEvent(e)

    def mousePressEvent(self, e):
        self.label.setText("mousePressEvent")
        # super().mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        self.label.setText("mouseReleaseEvent")
        # super().mouseReleaseEvent(e)

    def mouseDoubleClickEvent(self, e):
        self.label.setText("mouseDoubleClickEvent")
        # super().mouseDoubleClickEvent(e)

        # e.accept()
        # e.ignore()


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
