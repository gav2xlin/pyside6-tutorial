from PySide6.QtWidgets import QMainWindow, QApplication

import pyqtgraph as pg
import sys


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()

        self.graphWidget = pg.PlotWidget()
        self.setCentralWidget(self.graphWidget)

        hour = [1,2,3,4,5,6,7,8,9,10]
        temperature = [30,32,34,32,33,31,29,32,35,45]

        # self.graphWidget.setBackground('#bbccaa')
        # self.graphWidget.setBackground((100,50,255))      # RGB each 0-255
        # self.graphWidget.setBackground((100,50,255,25))   # RGBA (A = alpha opacity)
        # self.graphWidget.setBackground(QtGui.QColor(100,50,254,25))
        # color = self.palette().color(QtGui.QPalette.Window)  # Get the default window background,
        # self.graphWidget.setBackground(color)

        self.graphWidget.setBackground('w')
        self.graphWidget.plot(hour, temperature)


app = QApplication(sys.argv)
main = MainWindow()
main.show()
app.exec()
